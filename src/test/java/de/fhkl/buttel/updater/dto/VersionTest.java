package de.fhkl.buttel.updater.dto;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class VersionTest {

    Version a1 = new Version("1.2-SNAPSHOT");
    Version a2 = new Version("1.2");

    Version b = new Version("1.2.3");
    Version b1 = new Version("1.2.3");
    Version b2 = new Version("1.2.4");
    Version b3 = new Version("1.2.3.1");

    Version c1 = new Version("1.2.30");
    Version c2 = new Version("1.2.33");

    Version d1 = new Version("1.20.3");
    Version d2 = new Version("1.2.30");

    Version e1 = new Version("1.22.333.4444.55555.666666");
    Version e2 = new Version("1.2.3.4.5.6-SNAPSHOT");

    @Test
    public void testConstructor() throws Exception {
        assertThat(a1.getVersion()).isEqualTo("1.2");
        assertThat(a1.isSnapshot()).isTrue();

        assertThat(a2.getVersion()).isEqualTo("1.2");
        assertThat(a2.isSnapshot()).isFalse();
    }


    @Test
    public void testToInt() throws Exception {

        assertThat(e1.toInt()).isEqualTo(new int[]{1, 22, 333, 4444, 55555, 666666});
        assertThat(e2.toInt()).isEqualTo(new int[]{1, 2, 3, 4, 5, 6});
    }

    @Test
    public void testToString() throws Exception {
        assertThat(e1.toString()).isEqualTo("1.22.333.4444.55555.666666");
        assertThat(e2.toString()).isEqualTo("1.2.3.4.5.6-SNAPSHOT");
    }

    @Test
    public void testCompareTo() throws Exception {
        assertThat(a1.compareTo(a2)).isNegative();

        assertThat(b.compareTo(b1)).isZero();

        assertThat(b.compareTo(b2)).isNegative();

        assertThat(b.compareTo(b3)).isNegative();

        assertThat(c1.compareTo(c2)).isNegative();

        assertThat(d1.compareTo(d2)).isPositive();

        assertThat(b3.compareTo(b)).isPositive();
    }

    @Test
    public void testEquals() throws Exception {
         assertThat(b.equals(b1)).isTrue();
        assertThat(b.equals(b2)).isFalse();
    }
}
