package de.fhkl.buttel.updater;

import de.fhkl.buttel.updater.services.UpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class Updater {

    private static final Logger logger = LoggerFactory.getLogger(Updater.class);

    private final UpdateService updateService;
    private final boolean isOnline;
    private final boolean applicationExists;

    private Updater() {
        updateService = new UpdateService();

        applicationExists = checkApp();
        isOnline = updateService.isOnline();
    }

    public static void main(String... args) {
        logger.info("starting invoker");
        new Updater().start();
    }

    /**
     * execution of the update-process
     */
    private void start() {
        if (isOnline) {
            logger.debug("internet connection seems to be available");
            if (applicationExists) {
                checkForUpdates();
            } else {
                updateService.initialDownload();
            }
        } else {
            if (applicationExists) {
                logger.debug("no internet connection available. not checking for updates.");
            } else {
                logger.info("No application and no internet connection. Shutting down...");
                JOptionPane.showMessageDialog(null, "Die Anwendung ist nicht vorhanden oder fehlerhaft. " +
                        "\nBitte verbinden Sie sich mit dem Internet damit die Anwendung heruntergeladen werden kann.");
                System.exit(0);
            }
        }
        launchApp();
    }

    /**
     * opens a dialog whether or not to update if updates are available
     */
    private void checkForUpdates() {

        if (updateService.checkForUpdates()) {

            if (JOptionPane.showOptionDialog(null, "Möchten Sie aktualisieren?", "Updater",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    new String[]{"Ja", "Nein"}, null) == JOptionPane.YES_OPTION) {
                updateService.update();
            }
        }
    }

    /**
     * <p>returns true if the application maintained by the updater exists.</p>
     *
     * <p>if the application executable exists it is also checked if it is not empty</p>
     *
     * @return true if the application maintained by the updater exists
     */
    private boolean checkApp() {

        logger.debug("check if application '" + updateService.APP_NAME + "' exists");
        File app = new File(updateService.APP_NAME + ".jar");

        if (app.exists() && !app.isDirectory()) {
            logger.debug("application exists...");
            if (updateService.getLocalModules().isEmpty()) {
                logger.debug("...but is empty!");
                return false;
            } else {
                logger.debug("...and is not empty!");
                return true;
            }
        } else {
            logger.debug("application does not exist");
            return false;
        }
    }

    /**
     * launches the app located in same directory as the updater
     */
    private void launchApp() {
        logger.info("starting application");
        Runtime runtime = Runtime.getRuntime();

        try {
            runtime.exec("java -jar " + updateService.APP_NAME + ".jar");
        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("ExceptionMessage: " + e.getMessage());
        }
    }
}
