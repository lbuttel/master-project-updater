package de.fhkl.buttel.updater.dto;

/**
 * represents a version of a {@link de.fhkl.buttel.updater.dto.Module}
 *
 */
public class Version implements Comparable<Version> {

    private final String version;
    private final boolean isSnapshot;

    private static final String VERSION_SPLITTER = "\\.";
    private static final String SNAPSHOT = "-SNAPSHOT";

    public Version(String version) {

        if (version.endsWith(SNAPSHOT)) {
            isSnapshot = true;
            version = version.replace(SNAPSHOT, "");
        } else {
            isSnapshot = false;
        }

        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public boolean isSnapshot() {
        return isSnapshot;
    }

    public int[] toInt() {
        String[] versionStringArray =  version.split(VERSION_SPLITTER);

        int stringArrayLength = versionStringArray.length;

        int[] versionIntArray = new int [stringArrayLength];

        for (int i = 0; i < stringArrayLength; i++) {
            versionIntArray[i] = Integer.parseInt(versionStringArray[i]);
        }

        return versionIntArray;
    }

    @Override
    public String toString() {

        return isSnapshot ? (version + SNAPSHOT) : version;
    }

    @Override
    public int compareTo(Version o) {

        if (equals(o)) {
            return 0;
        }

        int[] thisIntArray = toInt();
        int[] otherIntArray = o.toInt();

        int minLength = Math.min(thisIntArray.length, otherIntArray.length);

        for (int i = 0; i < minLength; i++) {
            if (thisIntArray[i] < otherIntArray[i]) {
                return -1;
            } else if (thisIntArray[i] > otherIntArray[i]) {
                return 1;
            }
        }

        if (thisIntArray.length == otherIntArray.length) {
            if (isSnapshot) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (thisIntArray.length < otherIntArray.length) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        Version version1 = (Version) obj;

        return (isSnapshot == version1.isSnapshot()) && version.equals(version1.getVersion());

    }

    @Override
    public int hashCode() {
        int result = version.hashCode();
        result = 31 * result + (isSnapshot ? 1 : 0);
        return result;
    }
}
