package de.fhkl.buttel.updater.dto;

/**
 * represents a maven artifact
 */
public class Module {

    private final String groupId;
    private final String artifactId;
    private String classifier;
    private Version version;

    public Module(String groupId, String artifactId, String version) {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = new Version(version);
        classifier = "";
    }

    public Module(String groupId, String artifactId, String classifier, String version) {
        this(groupId, artifactId, version);
        this.classifier = classifier;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public Version getVersion() {
        return version;
    }

    public String getClassifier() {
        return classifier;
    }

    @Override
    public String toString() {

        if (classifier.isEmpty()) {
            return groupId + ':' + artifactId + ':' + version.toString();
        } else {
            return groupId + ':' + artifactId + '-' + classifier + ':' + version.toString();
        }
    }

    /**
     * compares this Module to the specified Module ignoring the version
     *
     * @param obj the object to compare this Module against
     * @return true if groupId and artifactId are equivalent
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        Module module = (Module) obj;

        return artifactId.equals(module.getArtifactId()) && groupId.equals(module.getGroupId()) && classifier.equals(module.getClassifier());

    }

    @Override
    public int hashCode() {
        int result = groupId.hashCode();
        result = 31 * result + artifactId.hashCode();
        result = 31 * result + classifier.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }
}
