package de.fhkl.buttel.updater.services;

import de.fhkl.buttel.updater.dto.Module;
import net.lingala.zip4j.exception.ZipException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * service class to handle the update process
 */
public class UpdateService {

    private static final Logger logger = LoggerFactory.getLogger(UpdateService.class);
    private static final String PROPERTIES_FILE = "updater.properties";

    private Properties updateProperties;
    private final DownloadService downloadService;
    private final FileService fileService;
    public String APP_NAME;
    private String APP_GROUPID;
    private String APP_ARTIFACTID;
    private List<Module> outdatedModules;

    /**
     * <p>constructor for UpdateService.</p>
     *
     * <p>if 'updater.properties' can not be loaded crucial information is missing.
     * in this case the program closes with exit-code 1</p>
     */
    public UpdateService() {

        try {
            updateProperties = getUpdateProperties();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("could not load properties from " + PROPERTIES_FILE + ". Aborting...");
            System.exit(1);
        }

        readAppProperties();

        fileService = new FileService(updateProperties);
        downloadService = new DownloadService(fileService, updateProperties);
    }

    /**
     * loads and returns the properties from 'updater.properties'
     *
     * @return the properties from 'updater.properties'
     * @throws IOException if properties can not be loaded
     */
    private Properties getUpdateProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(UpdateService.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE));
        return properties;
    }

    /**
     * <p>helper method for the constructor</p>
     * <p>reads properties from 'updater.properties'</p>
     */
    private void readAppProperties() {
        APP_NAME = updateProperties.getProperty("app.name", "app");
        APP_GROUPID = updateProperties.getProperty("app.groupId");
        APP_ARTIFACTID = updateProperties.getProperty("app.artifactId");
    }

    /**
     * performs an update
     */
    public void update() {
        updateModules(outdatedModules);
    }

    /**
     * updates the modules contained in the list {@code outdatedModules}
     * @param outdatedModules the list of modules to be updated
     */
    private void updateModules(List<Module> outdatedModules) {
        logger.info("starting update");

        applyUpdates(downloadUpdates(outdatedModules));

        cleanUp();
    }

    /**
     * takes a list of update-files and applies them to the application
     * @param updateFiles the list of paths to the update-files
     */
    private void applyUpdates(List<String> updateFiles) {
        logger.info("\tapplying updates...");
        try {
            fileService.updateJar('.' + File.separator + APP_NAME + ".jar", updateFiles);
        } catch (ZipException e) {
            e.printStackTrace();
            logger.warn("failed to update application. Aborting update process." +
                    "\nExceptionMessage: " + e.getMessage());
        }
        logger.info("update finished. cleaning up...");
    }

    /**
     * downloads the modules contained in the list {@code outdatedModules}
     * and returns a list with the paths of the downloaded files
     *
     * @param outdatedModules the list of modules to be downloaded
     * @return a list containing the paths to the downloaded modules
     */
    private List<String> downloadUpdates(List<Module> outdatedModules) {
        List<String> updateFiles = new ArrayList<>();

        try {
            logger.info("\tdownloading updates...");
            for (Module outdatedModule : outdatedModules) {
                String updateFile = downloadService.download(outdatedModule, true, FileService.TMP_FOLDER, outdatedModule.getArtifactId());
                updateFiles.add(updateFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("exception occurred during update-download. Aborting update process." +
                    "\nExceptionMessage: " + e.getMessage());
        }
        return updateFiles;
    }

    /**
     * returns true if any updates are available
     * @return true if any updates are available
     */
    public boolean checkForUpdates() {
        logger.info("check for updates...");

        try {
            outdatedModules = getOutdatedModules();

            if (outdatedModules.isEmpty()) {
                logger.info("no updates found");
                return false;
            } else {
                logger.info("updates found");
                return true;
            }

        } catch (IllegalStateException e) {
            e.printStackTrace();
            logger.warn("ExceptionMessage: " + e.getMessage());
            return false;
        }
    }

    /**
     * <p>returns a list of outdated modules</p>
     *
     * <p>an outdated module is a module with: localVersion < remoteVersion</p>
     *
     * @return a list of outdated modules
     * @throws IllegalStateException
     */
    private List<Module> getOutdatedModules() throws IllegalStateException {
        List<Module> localModules = getLocalModules();

        List<Module> remoteModules = getRemoteModules(localModules);

        List<Module> outdatedModules = new ArrayList<>();

        logger.debug("\tcompare local modules with remote modules to find outdated modules");
        for (int i = 0; i < localModules.size(); i++) {
            Module local = localModules.get(i);
            Module remote = remoteModules.get(i);

            if (local.equals(remote)) {
                if (local.getVersion().compareTo(remote.getVersion()) < 0) {
                    logger.info("\t\t" + local + " is outdated");
                    outdatedModules.add(remote);
                }
            }
        }

        return outdatedModules;
    }

    /**
     * returns a list of the modules inside the application
     * @return a list of the modules inside the application
     */
    public List<Module> getLocalModules() {
        return fileService.getLocalModules(APP_NAME + ".jar");
    }

    /**
     * returns a list containing the latest versions of the modules inside {@code localModules}
     *
     * @param localModules the list of modules contained in the application
     * @return list of modules fetched from nexus
     * @throws IllegalStateException if the request to nexus fails
     */
    private List<Module> getRemoteModules(List<Module> localModules) throws IllegalStateException {
        logger.debug("\tget corresponding remote module-versions...");
        List<Module> remoteModules = new ArrayList<>();

        for (Module module : localModules) {
            Module remoteModule = downloadService.fetchInformation(module);
            remoteModules.add(remoteModule);
        }

        return remoteModules;
    }

    /**
     * downloads the whole application
     */
    public void initialDownload() {
        Module module = new Module(APP_GROUPID, APP_ARTIFACTID, "-1.-1");

        try {
            logger.info("downloading application...");
            downloadService.download(module, true, "", APP_NAME);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("exception occurred during application download" +
                    "\nExceptionMessage: " + e.getMessage());
        }
    }

    /**
     * removes files and folders created during the update process
     */
    private void cleanUp() {
        fileService.cleanUp();
    }

    /**
     * returns true if a connection to nexus-server can be established
     * @return true if a connection to nexus-server can be established
     */
    public boolean isOnline() {
        return downloadService.isOnline();
    }
}