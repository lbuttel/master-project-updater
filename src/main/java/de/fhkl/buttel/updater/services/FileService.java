package de.fhkl.buttel.updater.services;

import de.fhkl.buttel.updater.dto.Module;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * service class to handle all interactions with the filesystem
 */
public class FileService {
    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    static String TMP_FOLDER;
    private static String TMP_UNPACKED;

    private static String BASE_GROUPID;

    public FileService(Properties updateProperties) {
        TMP_FOLDER = updateProperties.getProperty("update.folder") + File.separator;
        TMP_UNPACKED = TMP_FOLDER + "unpacked" + File.separator;
        BASE_GROUPID = updateProperties.getProperty("app.groupId.base");
    }

    /**
     * extracts a jar to {@code TMP_FOLDER}
     *
     * @param pathToJar
     * @throws ZipException
     */
    private void extractJar(String pathToJar) throws ZipException {

        ZipFile jar = new ZipFile(pathToJar);

        logger.debug("\t\t\textracting " + jar.getFile().getName() + "...");
        jar.extractAll(TMP_UNPACKED);
        logger.debug("\t\t\t\t" + pathToJar + " successfully extracted to " + TMP_UNPACKED);
    }

    /**
     * updates {@code jarToUpdate} with the content of the files inside the list {@code updateFiles}
     *
     * @param jarToUpdate the path to the jar which should be updated
     * @param updateFiles the list of paths to the update-jars
     * @throws ZipException if update-jars can not be extracted or update-jars can not be added to {@code jarToUpdate}
     */
    public void updateJar(String jarToUpdate, List<String> updateFiles) throws ZipException {
        logger.debug("\t\ttry to update " + jarToUpdate);
        for (String updateFile : updateFiles) {
            extractJar(updateFile);
        }

        ZipFile app = new ZipFile(jarToUpdate);

        addExtractedClassesToJar(app, TMP_UNPACKED);
        addExtractedMetaInfToJar(app, TMP_UNPACKED);
        logger.debug("\t\tsuccessfully updated " + jarToUpdate);
    }

    /**
     * adds all files from the update to {@code jarToUpdate}
     *
     * @param jarToUpdate the path to the jar which should be updated
     * @param extractedJar the path to the folder containing the update-files
     * @throws ZipException if files can not be added to {@code jarToUpdate}
     */
    private void addExtractedClassesToJar(ZipFile jarToUpdate, String extractedJar) throws ZipException {

        ZipParameters parameters = new ZipParameters();

        File[] files = new File(extractedJar).listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                if (!file.getName().equals("META-INF")) {
                    jarToUpdate.addFolder(file, parameters);
                } else {
                    continue;
                }
            } else {
                jarToUpdate.addFile(file, parameters);
            }
            logger.debug("\t\t\tsuccessfully added " +  file.getPath());
        }
    }

    /**
     * <p>adds META-INF from the update to {@code jarToUpdate} without overwriting the MANIFEST.MF</p>
     *
     * <p>(META-INF of the update contains latest 'pom.properties')</p>
     *
     * @param jarToUpdate the path to the jar which should be updated
     * @param extractedJar the path to the folder containing the update-files
     * @throws ZipException if files can not be added to {@code jarToUpdate}
     */
    private void addExtractedMetaInfToJar(ZipFile jarToUpdate, String extractedJar) throws ZipException {
        ZipParameters parameters = new ZipParameters();

        String propertiesPath = extractedJar + "META-INF" + File.separator + "maven" + File.separator;
        parameters.setRootFolderInZip("META-INF");
        jarToUpdate.addFolder(propertiesPath, parameters);
        logger.debug("\t\t\tsuccessfully added " + propertiesPath);
    }

    /**
     * returns a list of all non-3rd-party modules inside {@code jarFile}
     *
     * @param jarFile the path to the jar-file
     * @return a list of all non-3rd-party modules inside {@code jarFile}
     */
    public List<Module> getLocalModules(String jarFile) {
        logger.debug("\tget local module-versions...");

        List<Module> modules = new ArrayList<>();

        List<Properties> propertiesList = getModuleProperties(jarFile);

        for (Properties properties : propertiesList) {
            String groupId = properties.getProperty("groupId");
            String artifactId = properties.getProperty("artifactId");
            String version = properties.getProperty("version");

            Module module = new Module(groupId, artifactId, version);
            logger.debug("\t\tfound module " + module);

            if (module.getGroupId().contains(BASE_GROUPID)) {
                modules.add(module);
            } else {
                logger.debug("\t\t\tignoring because module is a 3rd party library");
            }
        }

        return modules;
    }

    /**
     * return a list of all occurrences of 'pom.properties' inside {@code jarFile}
     *
     * @param jarFile the path to the jar-file
     * @return a list of all 'pom.properties'-entries in {@code jarFile}
     */
    private List<Properties> getModuleProperties(String jarFile) {
        logger.debug("\t\tread module-properties from " + jarFile);

        List <Properties> propertiesList = new ArrayList<>();

        try (JarFile jar = new JarFile(jarFile)) {

            Enumeration<JarEntry> jarEntries = jar.entries();
            Properties properties;

            while (jarEntries.hasMoreElements()) {
                JarEntry jarEntry = jarEntries.nextElement();

                if (jarEntry.getName().endsWith("pom.properties")) {
                    properties = new Properties();

                    logger.debug("\t\t\tfound properties at " + jarEntry.getName());
                    properties.load(jar.getInputStream(jarEntry));

                    propertiesList.add(properties);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();

            logger.warn("could not open jarfile " + jarFile +
                    "\nExceptionMessage: " + e.getMessage());
        }

        return propertiesList ;
    }

    /**
     * writes an inputstream to {@code destDir}/{@code filename}
     *
     * @param inputStream the inputstream to be saved
     * @param destDir the directory where the inputstream is saved to
     * @param filename the filename of the saved file
     * @return the path of the saved file
     * @throws IOException if writing to disk fails
     */
    public String writeToFile(InputStream inputStream, String destDir, String filename) throws IOException {

        ensureThatDirExists(destDir);

        String filePath = destDir + filename + ".jar";

        logger.debug("\t\t\twrite InputStream from response to " + filePath);

        FileOutputStream outputStream = new FileOutputStream(filePath);

        int aByte;
        while ((aByte = inputStream.read()) != -1) {
            outputStream.write(aByte);
        }

        outputStream.close();

        return filePath;
    }

    /**
     * checks if a directory exists and if not creates it including all not existing parent directories
     * @param destDir the path to the folder to be checked
     */
    private void ensureThatDirExists(String destDir) {
        File dir = new File(destDir);

        if (!dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    /**
     * removes files and folders created during the update process
     */
    public void cleanUp() {
        deleteFolder(new File(TMP_FOLDER));
        logger.debug("\tdeleted " + TMP_FOLDER);
    }

    /**
     * recursive method to delete a folder and all of its content
     * @param folder the folder to be deleted
     */
    private void deleteFolder(File folder) {

        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteFolder(file);
                } else {
                    file.delete();
                }
            }
        }
        folder.delete();
    }
}