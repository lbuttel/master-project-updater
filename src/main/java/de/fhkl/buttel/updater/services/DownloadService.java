package de.fhkl.buttel.updater.services;

import de.fhkl.buttel.updater.dto.Module;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

/**
 * service class to handle interactions with nexus server
 */
public class DownloadService {
    private static final Logger logger = LoggerFactory.getLogger(DownloadService.class);

    private static String NEXUS_PROTOCOL;
    private static String NEXUS_IP;
    private static String NEXUS_PORT;
    private static String NEXUS_PATH;
    private static final String REST_PATH = "/service/local/artifact/maven";
    private static String REPOSITORY;

    private final FileService fileService;

    public DownloadService(FileService fileService, Properties properties) {
        this.fileService = fileService;

        readServerProperties(properties);
    }

    /**
     * <p>helper method for the constructor</p>
     * <p>reads the server properties from 'updater.properties'</p>
     */
    private void readServerProperties(Properties properties) {
        NEXUS_PROTOCOL = properties.getProperty("nexus.protocol", "http");
        NEXUS_IP = properties.getProperty("nexus.ip", "127.0.0.1");
        NEXUS_PORT = properties.getProperty("nexus.port", "8081");
        NEXUS_PATH = properties.getProperty("nexus.path", "/nexus");
        REPOSITORY = properties.getProperty("nexus.repository", "releases");
    }

    /**
     * downloads the specified module and saves it to {@code destDir}/{@code destName}
     *
     * @param module the artifact to be downloaded
     * @param latest should it be the latest version?
     * @param destDir the directory where the artifact is downloaded to
     * @param destName the filename for the downloaded artifact
     * @return the path to the downloaded artifact
     * @throws IllegalStateException if request to nexus fails
     * @throws IOException if downloaded artifact can not be written to disk
     */
    public String download(Module module, boolean latest, String destDir, String destName) throws IllegalStateException, IOException {
        logger.debug("\t\tdownloading " + module);

        HttpEntity response = nexusRequest(module, latest, false);

        return fileService.writeToFile(response.getContent(), destDir, destName);
    }

    /**
     * returns the latest version of {@code localModule} available from nexus
     *
     * @param localModule module to get the latest version of
     * @return the latest version of {@code localModule} available from nexus
     * @throws IllegalStateException if request to nexus fails
     */
    public Module fetchInformation(Module localModule) throws IllegalStateException {

        logger.debug("\t\tfetch information for " + localModule + " from nexus-server");

        HttpEntity response = nexusRequest(localModule, true, true);

        try {
            logger.debug("\t\t\tparse response to xml");
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(response.getContent());

            return getModuleFromXml(doc);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
            logger.warn("ExceptionMessage: " + e.getMessage());
        }
        throw new IllegalStateException("response parsing failed. Update not possible.");
    }

    /**
     * returns a Module-object created from the details retrieved from a nexus request
     *
     * @param doc xml-response of the nexus request
     * @return a Module-object created from the details retrieved from a nexus request
     */
    private Module getModuleFromXml(Document doc) {

        logger.debug("\t\t\tget gav-coordinates from xml...");
        String groupId = doc.getElementsByTagName("groupId").item(0).getTextContent();
        String artifactId = doc.getElementsByTagName("artifactId").item(0).getTextContent();
        String version = doc.getElementsByTagName("version").item(0).getTextContent();

        Module module = new Module(groupId, artifactId, version);
        logger.debug("\t\t...got " + module);

        return module;
    }

    /**
     * performs a http get request to the nexus server
     *
     * @param module the requested artifact
     * @param latest flag to define if version should be latest
     * @param onlyDetails flag to define if only details about the artifact should be fetched
     * @return the http response if the request was successfully
     * @throws IllegalStateException if the request to nexus fails
     */
    private HttpEntity nexusRequest(Module module, boolean latest, boolean onlyDetails) throws IllegalStateException {

        String rest = onlyDetails ? "/resolve" : "/redirect";
        String groupId = module.getGroupId();
        String artifactId = module.getArtifactId();
        String classifier = module.getClassifier();
        String version = latest ? "LATEST" : module.getVersion().toString();

        try {
            HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(5000)
                    .setConnectionRequestTimeout(5000)
                    .setSocketTimeout(5000)
                    .build();
            HttpClient client = httpClientBuilder.setDefaultRequestConfig(requestConfig).build();

            URI uri = new URIBuilder()
                    .setScheme(NEXUS_PROTOCOL)
                    .setHost(NEXUS_IP)
                    .setPort(Integer.parseInt(NEXUS_PORT))
                    .setPath(NEXUS_PATH + REST_PATH + rest)
                    .setParameter("g", groupId)
                    .setParameter("a", artifactId)
                    .setParameter("c", classifier)
                    .setParameter("v", version)
                    .setParameter("r", REPOSITORY)
                    .build();

            HttpGet request = new HttpGet(uri);

            logger.debug("\t\t\tperforming http-GET-request: " + uri);
            HttpResponse response = client.execute(request);

            if (response.getStatusLine().getStatusCode() == 200) {
                logger.debug("\t\t\t\thttp request successful");
                return response.getEntity();
            } else {
                logger.debug("\t\t\t\thttp request failed with status code: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
            }


        } catch (URISyntaxException e) {
            e.printStackTrace();

            logger.warn("URL: " + NEXUS_IP + ':' + NEXUS_PORT + NEXUS_PATH + REST_PATH + rest +
                    "?g=" + groupId +
                    "&a=" + artifactId +
                    "&c=" + classifier +
                    "&v=" + version +
                    "&r=" + REPOSITORY +
                    " is malformed" +
                    "\nExceptionMessage: " + e.getMessage());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            logger.warn("ExceptionMessage: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("ExceptionMessage: " + e.getMessage());
        }
        throw new IllegalStateException("Http request failed. Update not possible.");
    }

    /**
     * returns true if the updater can connect to the update server
     * @return true if the updater can connect to the update server
     */
    public boolean isOnline() {
        logger.debug("try to connect to update server " + NEXUS_IP + "...");

        try {
            URL url = new URL("http://" + NEXUS_IP);
            URLConnection con = url.openConnection();
            con.setConnectTimeout(10000);
            con.connect();
        }
        catch( Exception ex ) {
            logger.warn("can not connect to http://" + NEXUS_IP + ". client or host offline.");
            return false;
        }
        return true;
    }
}